﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// manages all the ants!
public class AntManager : MonoBehaviour
{
    public int numAntsToCreate;   // how many ants to create at startup
    public int antRowCount;       // how many ants per row

    public float antColumnGap = 0.5f;
    public float antRowGap = 0.5f;
    public Vector3 curAntPos = Vector3.zero;

    public GameObject antPrefab;       // this is the prefab used to instantiate ants at runtime dragged from Assets/Ant/Prefabs

    // Start is called before the first frame update
    void Start()
    {
        // make a bunch of ants
        int xCount = 0;
        for (int i = 0; i < numAntsToCreate; i++)
        {
            // instantiate a new ant!
            // we are passing in our transform since this script is attached to Ant Containter GameObject in the Scene
            // thus transfrom of Ant Containter is the parent transfrom of every ant created.
            // if we scale or translate Ant Container all the child ant game objects will be scaled as children of the transform
            GameObject newGameObjectInstance = GameObject.Instantiate(antPrefab, transform);

            // place ant locally in the containter
            newGameObjectInstance.transform.localPosition = curAntPos;
            // newGameObjectInstance is the GameObject instance just created from the antPrefab, we should probably save it off somewhere

            // update for next ants start position
            xCount++;
            if (xCount > antRowCount)
            {
                curAntPos.x -= antColumnGap * (float)antRowCount;
                curAntPos.z += antRowGap;
                xCount = 0;
            }
            else
            {
                curAntPos.x += antColumnGap;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
