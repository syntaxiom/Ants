﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntLogic : MonoBehaviour
{
    public enum State
    {
        IDLE,
        MOVING,
        SCORING,
        DEAD
    };

    public float distance = 0.5f;
    public bool isGrounded;
    public Vector3 range;
    public Vector3 force;       // force vectors set in inspector]]

    public Vector3 turnForce;       // rotation force vectors set in inspector
    public float turningDuration; // amount of time to apply the rotation force


    public Rigidbody body;  // physics engine needs this to work, set reference in inspector

    public State currentState = AntLogic.State.IDLE;

    public float deciscionInterval = 3.0f;  // how often to make a turn decision

    private float turningTimer = 0.0f;      // how much time to apply turnng force
    private float turnDecisionTimer = 0.0f; // current time spent turning

    private float turnDirection;

    // Start is called before the first frame update
    /*
     * Start is called after all objects are initialized so you can safely speak to other objects or query them using eg. 
     * GameObject.FindWithTag. Each GameObject's Awake is called in a random order between objects. 
     * Because of this, you should use Awake to set up references between scripts,
     * and use Start() to pass any information back and forth. Awake is always called before any Start functions. 
     * This allows you to order initialization of scripts. Awake can not act as a coroutine.
    */
    void Start()
    {
       // Debug.LogError("Antlogic : Start called!");       // TBD remove after you get sick of it spamming the console
    }

    /*
     Awake is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    Like the Start function, Awake is called exactly once in the lifetime of the script. However, 
    Awake is called when the script object is initialised, regardless of whether or not the script is enabled.
    Start may not be called on the same frame as Awake if the script is not enabled at initialisation time.  
    */
    void Awake()
    {
      //  Debug.LogError("Antlogic : Awake called!");       // TBD remove after you get sick of it spamming the console
    }

    /* Update runs once per frame.FixedUpdate can run once, zero,
     * or several times per frame, depending on how many physics frames per second are set in the time settings,
     * and how fast/slow the framerate is.
     */
    void Update()
    {
        if (turnDecisionTimer >= 0 && turningTimer <= 0)
        {
            turnDecisionTimer -= 1.0f * Time.deltaTime;
            if (turnDecisionTimer < 0.0f)
            {
                body.velocity = Vector3.zero;   // stop forward movement at start of turn, a pro would add force in oppisite direction

                turnDecisionTimer = deciscionInterval;  // reset back

                turningTimer = turningDuration; // set the timer for the time we will spend turning

                // decide of left or right turn
                turnDirection = Random.Range(0, 2) > 0 ? -1.0f : 1.0f;
            }
        }

        if (turningTimer > 0)
        {
            turningTimer -= 1.0f * Time.deltaTime;
        }
    }

    // do physics based movement here so in sync with Unity physics engine.
    // this is called at a fixed rate, 
    private void FixedUpdate()
    {
        Vector3 v = Vector3.zero;

        Vector3 curLoc = transform.localRotation.eulerAngles;

        if (currentState == State.MOVING)
        {
            /*
            GroundCheck();

            // we are too close to the ground so add some force and push up a little
            if (isGrounded)
            {
                v.x = check(range.x, curLoc.x, force.x);
                v.y = check(range.y, curLoc.y, force.y);
                v.z = check(range.z, curLoc.z, force.z);
            }
            */


            if (turningTimer > 0)
            {
                body.AddRelativeTorque(turnDirection * turnForce * Time.fixedDeltaTime);
            }
            //else
            {
                body.AddRelativeForce(force * Time.fixedDeltaTime);
            }
        }

    }

    private float check(float r, float cur, float force)
    {
        // does nothing, but I had a plan
        return force;
    }

    // raycast down on y axis to see if we are  close to the table
    void GroundCheck()
    {
        RaycastHit hit;
        Vector3 dir = new Vector3(0, -1);

        if (Physics.Raycast(transform.position, dir, out hit, distance))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
    }
}
