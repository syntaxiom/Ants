﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeLogic : MonoBehaviour
{
    public enum State
    {
        IDLE,
        MOVING,
        SCORING,
        DEAD
    };

    public Rigidbody body;  // physics engine needs this to work, set reference in inspector

    public State currentState = CubeLogic.State.IDLE;

    public float movementForce = 1.0f;   // amount of force to apply when moving, only change this value in inspecgtor in editor, 100.f is only used first time it is dragged into project

    private float turnDecisionTimer = 0.0f;

    public float deciscionInterval = 0.5f;

    public float rotateSpeed = 1.0f;

    public float turnByAmount = 45.0f;

    private Vector3 moveVector = new Vector3(0.0f, 0.0f, 0.0f);

    private bool doTurn;
    


    // Start is called before the first frame update
    /*
     * Awake is called after all objects are initialized so you can safely speak to other objects or query them using eg. 
     * GameObject.FindWithTag. Each GameObject's Awake is called in a random order between objects. 
     * Because of this, you should use Awake to set up references between scripts,
     * and use Start() to pass any information back and forth. Awake is always called before any Start functions. 
     * This allows you to order initialization of scripts. Awake can not act as a coroutine.
    */
    void Start()
    {
        //Debug.LogError("Antlogic : Start called!");       // TBD remove after you get sick of it spamming the console
    }

    /*
     Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    Like the Awake function, Start is called exactly once in the lifetime of the script. However, 
    Awake is called when the script object is initialised, regardless of whether or not the script is enabled.
    Start may not be called on the same frame as Awake if the script is not enabled at initialisation time.  
    */
    void Awake()
    {
        //Debug.LogError("Antlogic : Awake called!");       // TBD remove after you get sick of it spamming the console
    }

    /* Update runs once per frame.FixedUpdate can run once, zero,
     * or several times per frame, depending on how many physics frames per second are set in the time settings,
     * and how fast/slow the framerate is.
     */
    void Update()
    {
        if (turnDecisionTimer >= 0)
        {
            turnDecisionTimer -= 1.0f * Time.deltaTime;
            if (turnDecisionTimer < 0.0f)
            {
                Debug.Log("turning");
                turnDecisionTimer = deciscionInterval;  // reset back
                doTurn = true;
            }
        }
    }

    // do physics based movement here so in sync with Unity physics engine.
    // this is called at a fixed rate, 
    private void FixedUpdate()
    {
        if (doTurn)
        {
            if (transform.localEulerAngles.y == turnByAmount)
            //if (false)
            {
                doTurn = false;
                doRandomTurn();
            }
            else
            {
                Turn();
            }
        }

        if (currentState == State.MOVING)
        {
            moveVector = Quaternion.Euler(transform.localRotation.eulerAngles) * Vector3.forward;

            if (body != null)
            {
                body.AddRelativeForce(moveVector * movementForce * Time.fixedDeltaTime);    // note how it's scaled by time, so it stays same speed if we muck with physics settings framerate
            }
        }
    }

    private void doRandomTurn()
    {
        turnByAmount = Random.Range(-360.0f, 360.0f);

        if (body != null)
        {
            body.velocity = Vector3.zero;   // cheat the physics and stop moving.
        }
    }


    private void Turn()
    {
        float curAmount = rotateSpeed;

        if (turnByAmount - transform.localEulerAngles.y < 0.0f)
        {
            curAmount = -curAmount;
        }

        //curAmount *= Time.fixedDeltaTime;

        transform.Rotate(0.0f,  curAmount, 0.0f);
    }
}
